import scala.math._
import scala.io.Source
import collection.mutable
import collection.mutable._
import scala.sys.process._
import java.io._
import java.io.File;
import java.io.IOException;
import scala.collection.JavaConversions._
import scala.util.Sorting 
import scala.util.Random

import htsjdk.samtools.DefaultSAMRecordFactory;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SamInputResource;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.ValidationStringency;
import htsjdk.samtools.seekablestream.SeekableStream;

import breeze.stats._
import breeze.stats.distributions._
import breeze.stats.DescriptiveStats._
import breeze.math._
import breeze.linalg._
//import breeze.plot._
import breeze.numerics.{exp, log, abs, sqrt, I, NaN}

import java.awt.Color

import breeze.generic.UFunc
import breeze.generic.MappingUFunc


//the Matrix sparsification Ufunc that's used to fit a sparse Ising model
object sparsify extends UFunc with MappingUFunc {
  implicit object implDouble extends Impl2[Double, Double, Double] {
    def apply(a: Double, alpha: Double) = if (a > 0.0) scala.math.max(0, a - alpha ) else scala.math.min(0, a + alpha )
  }
}




case class SimpleRead(chr:String, startpos:Long, endpos:Long, negstrand:Boolean)

case class SampleRecord(fname:String, sampleid:String, celltype:String, 
                        bamfh:BamFileReader)

case class SamplePileups(fname:String, sampleid:String, celltype:String, 
                        pileup:Double )

object CelltypeOrdering extends Ordering[SampleRecord] {
  def compare(a:SampleRecord, b:SampleRecord) = a.celltype compare b.celltype
}

object IndividualOrdering extends Ordering[SampleRecord] {
  def compare(a:SampleRecord, b:SampleRecord) = a.sampleid compare b.sampleid
}

object CelltypePileupOrdering extends Ordering[SamplePileups] {
  def compare(a:SamplePileups, b:SamplePileups) = a.celltype compare b.celltype
}

object IndividualPileupOrdering extends Ordering[SamplePileups] {
  def compare(a:SamplePileups, b:SamplePileups) = a.sampleid compare b.sampleid
}

class MemoryLogger() {
  private val runtime = Runtime.getRuntime()
  import runtime.{ totalMemory, freeMemory, maxMemory }

  def printMemoryStats() {
    val mb = 1024*1024
    System.out.println(
      "total memory = %s, max memory = %s, free memory = %s, used memory = %s".format(
      totalMemory/mb, maxMemory/mb, freeMemory/mb, (totalMemory-freeMemory)/mb    ))
    System.out.flush()
  }
}

class FilePrinter(f: java.io.File, append:Boolean = false) {
  var p:java.io.BufferedWriter = _
  openBufferedWriter()
  def openBufferedWriter() = {
    var fw = new java.io.FileWriter(f.getAbsoluteFile(), append);
    p = new java.io.BufferedWriter(fw)
  }
  def println(text:String) = {
    p.write(text)
    p.newLine() 
  }
  def closeBufferedWriter() = {
    p.close() 
  }
}




class BamFileReader(bamfn:String)  {

  var factory:SamReaderFactory = _
  var resource:SamInputResource = _
  var myReader:SamReader = _
  //var sequencelength:Long = 0
  var insertcount:Long = 0

  initializeReader()

  def initializeReader() = {
    //first get coverage differences
    factory = 
      SamReaderFactory.makeDefault().validationStringency(ValidationStringency.LENIENT
      ).enable(SamReaderFactory.Option.DONT_MEMORY_MAP_INDEX)
    resource = 
      SamInputResource.of(new File(bamfn)).index(new File(bamfn + ".bai" ))//bamfn.replaceAll(".bam$",".bai")))    
    myReader = factory.open(resource)
  }

  def getSequenceLength(chrom: String):Long = {
      return myReader.getFileHeader().getSequence(chrom).getSequenceLength().toLong
  }

  def getReads(chrom: String,startpos: Long, endpos: Long, readmode: String, cuts: Boolean):List[(Long,Long,Boolean)] = {

      var start_offset = 0.toLong
      var end_offset = 0.toLong
      if (readmode == "ATAC") {
        start_offset = -4.toLong  
        end_offset = 5.toLong
      }
    
    
      val samiterator = myReader.queryOverlapping(chrom,startpos.toInt+1,endpos.toInt)       
      val readIter:scala.collection.Iterator[SAMRecord] = samiterator
      val output = filterReadIterator(readIter, startpos.toInt + 1, endpos.toInt  ,start_offset.toInt, end_offset.toInt, cuts).map( 
                    a =>  SimpleRead(a.getReferenceName(),
                          a.getAlignmentStart().toLong-1.toLong + start_offset,
                          a.getAlignmentEnd().toLong-1.toLong+1.toLong + end_offset, a.getReadNegativeStrandFlag() )  ).toList.map(
                    b => (b.startpos, b.endpos, b.negstrand )).sorted
      samiterator.close()
      return output
  }


  def filterReadIterator(
    readIter:scala.collection.Iterator[SAMRecord], 
    startpos:Int,endpos:Int,start_offset:Int, end_offset:Int, cuts:Boolean):scala.collection.Iterator[SAMRecord] = {
   

    return readIter.filter(a => 

                        ( cuts && (!a.getReadNegativeStrandFlag() &&  
                                    a.getAlignmentStart() + start_offset.toInt >= startpos  && 
                                    a.getAlignmentStart() + start_offset.toInt <= endpos ||
                                    a.getReadNegativeStrandFlag() &&  
                                    a.getAlignmentEnd() + end_offset.toInt >= startpos  && 
                                    a.getAlignmentEnd() + end_offset.toInt <= endpos) ||
                          !cuts && (a.getAlignmentStart() + start_offset.toInt >= startpos && 
                                    a.getAlignmentStart() + start_offset.toInt <= endpos ||
                                    a.getAlignmentEnd() + end_offset.toInt >= startpos && 
                                    a.getAlignmentEnd() + end_offset.toInt <= endpos ||
                                    a.getAlignmentStart() + start_offset.toInt < startpos &&
                                    a.getAlignmentEnd() + end_offset.toInt > endpos )                         
                        ) &&
                        a.getMappingQuality() >= 8 &&
                        a.getMateReferenceIndex()  == a.getReferenceIndex() &&
                        !a.getReadUnmappedFlag() && 
                        !a.getMateUnmappedFlag() && 
                        (  a.getReadNegativeStrandFlag() && 
                          !a.getMateNegativeStrandFlag() || 
                          !a.getReadNegativeStrandFlag() && 
                           a.getMateNegativeStrandFlag())  &&
                        (  a.getReadNegativeStrandFlag() && 
                           a.getAlignmentStart() >= a.getMateAlignmentStart() || 
                           !a.getReadNegativeStrandFlag() && 
                           a.getAlignmentStart() <= a.getMateAlignmentStart()) // &&
                       )
                        //(a.getInferredInsertSize() < -30 || a.getInferredInsertSize()  > 30) ) //&&


  }
}




class MultiFileReader(chr:Option[String],chrstartpos:Option[Long], chrendpos:Option[Long],filenames:List[List[String]], 
                      windowSize:Long, bgSize:Long, readmode:String, cuts:Boolean) {
  
  var samples:List[SampleRecord] = _
  var currentpos:Long = 0
  var chrend:Long  = 0
  loadFiles()

  def loadFiles() = {
    samples = filenames.map(a => new SampleRecord(a(0), a(1), a(2), new BamFileReader(a(0))      ) )
    if (chr.isDefined) {
      chrend = getSequenceLength(chr.get)
    }
    if (chrstartpos.isDefined) {
      currentpos = chrstartpos.get
    }
    if (chrendpos.isDefined) {
      chrend = chrendpos.get
    }

    println(chrend)
  }


  def getSequenceLength(chrom: String):Long = {
    return samples(0).bamfh.getSequenceLength(chrom)
  }

  def hasNextRegion():Boolean = {
    return (currentpos + windowSize < chrend)
  }


  def getNextRegion():(List[SamplePileups],Long,Long) = {
    var output:List[SamplePileups]=List()
    var returnoutput:(List[SamplePileups],Long,Long) = (output, 0.toLong, 0.toLong)
    var testwindow = (currentpos, 
      scala.math.min(currentpos + windowSize, chrend ))
    
    if (hasNextRegion()) {
        returnoutput = getRegion(chr.get,testwindow._1, testwindow._2)
        currentpos = currentpos + windowSize
    }
    else {
        returnoutput = (output, testwindow._1,testwindow._2)
    }
    return returnoutput
  }

  //I'm using zero-based (e.g. BED files)
  //and feeding them into htsjdk which uses 1 based. 
  def getRegion(chrom:String,startpos:Long, endpos:Long):(List[SamplePileups],Long,Long) = {
    var output:List[SamplePileups]=List()
    
    //calculate start and end of background, taking into account edge of chromosomes
    var bgstartpos:Long = scala.math.min(startpos + windowSize/2.toLong - bgSize/2.toLong, 0.toLong)
    var bgendpos:Long = scala.math.min(bgstartpos + bgSize, getSequenceLength(chrom).toLong)
    bgstartpos = bgendpos - bgSize

    val pileups = samples.map(
      a => new SamplePileups(a.fname, a.sampleid, a.celltype,
      
        (a.bamfh.getReads(chrom,startpos, endpos,readmode, cuts).length.toDouble/(endpos-startpos).toDouble + 1.toDouble) /
        (a.bamfh.getReads(chrom,bgstartpos, bgendpos,readmode, cuts).length.toDouble/(bgendpos-bgstartpos).toDouble + 1.toDouble)
      
      ))
      output = pileups
    return (output, startpos, endpos)
  }

}






class IntermediateCalculationStore(sampleinfo:List[List[String]], sharing_ind:Int, 
                                   //pileupfn:String,
                                   mfr:MultiFileReader,
                                   edgefn:Option[String], ppeak:Double,numlntrainpeaks:Int,
                                   rrg:Option[RandomRegionGenerator], 
                                   regionfn:Option[String]
                                 ) {

  var edgeMatrix:DenseMatrix[Double] = DenseMatrix.zeros[Double](sampleinfo.length,sampleinfo.length)
  var sharingMatrix:DenseMatrix[Double] = DenseMatrix.zeros[Double](sampleinfo.length,sampleinfo.length)
  var bglognormal_mu = DenseVector.zeros[Double](sampleinfo.length)
  var bglognormal_sigma =  DenseVector.zeros[Double](sampleinfo.length)
  var peaklognormal_mu = DenseVector.zeros[Double](sampleinfo.length)
  var peaklognormal_sigma =  DenseVector.zeros[Double](sampleinfo.length)
  var peakprob = ppeak
  var sharingind = sharing_ind
  var sampleids:List[String] = _
  var regions:List[(String,Long,Long)] = _

  loadStore()

  def loadStore() = {

    if (sharing_ind != -1 ){
      sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","") + "$" + a(sharing_ind + 1) )
    }
    else{
      sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","") )
    }


    loadSharingMatrix()
    loadEdgeMatrix()
    loadPriorRegions()
    //fitLognormals()
  }

  def loadEdgeMatrix() = {
    if (edgefn.isDefined) {
      var edgeinfo = io.Source.fromFile(edgefn.get).getLines.toList.map(a => a.trim)
      var a = 0
      for( a <- 0 to sampleinfo.length - 1){
        edgeMatrix(a,::) := DenseVector(edgeinfo(a).split("\t").map(b => b.toDouble):_*).t   
      }
    }
  }

  def loadSharingMatrix() = {
    if (sharing_ind != -1 ){
      var types = sampleinfo.map(a => a(sharing_ind + 1) )
      var combs = types.zipWithIndex.toSeq.combinations(2).toList
      combs.map( a => sharingMatrix(a(0)._2, a(1)._2 ) = 1.0 )
      combs.map( a => sharingMatrix(a(1)._2, a(0)._2 ) = 1.0 )
    }
  }

  /*
  def loadLognormals() = {


    var bgpileup = DenseMatrix(
                    io.Source.fromFile(pileupfn).getLines.toList.map(a => a.trim).map(a => a.split("\t").map(b => b.toDouble) ):_*
                   )
    var bgpkcount:Int = scala.math.ceil(ppeak * bgpileup.rows).toInt
    var bgnopkcount:Int = scala.math.floor((1.0-ppeak) * bgpileup.rows).toInt
    
    println(bgpkcount)
    println(bgnopkcount)
 
    var bgpileup_pk = DenseMatrix.zeros[Double](bgpkcount,sampleinfo.length)
    var bgpileup_nopk =  DenseMatrix.zeros[Double](bgnopkcount,sampleinfo.length)
    
    //var pkinds = argtopk(bgpileup(::, *),bgpkcount)
    //var nopkinds = argtopk(bgpileup(::, *),bgnopkcount)

    var sortedinds:DenseVector[scala.collection.IndexedSeq[Int]] = argsort(bgpileup(::,*)).t

    var pkinds   = Range(0,sampleinfo.length).map(a => sortedinds(a).slice(0,bgpkcount)  )  //argtopk(bgpileup(::, *),bgpkcount)
    var nopkinds = Range(0,sampleinfo.length).map(a => sortedinds(a).drop(bgpkcount)  ) //argtopk(bgpileup(::, *),bgnopkcount)

    println(bgpileup_nopk.rows)
    println(bgpileup_pk.rows)
    //println(pkinds)
    //println(nopkinds)

    //top peak list
    //bottom nonpeak list
    var a = 0
    for( a <- 0 to sampleinfo.length - 1){
      bgpileup_pk(::,a) := bgpileup(pkinds(a),a) //.flatten()
      bgpileup_nopk(::,a) := bgpileup(nopkinds(a),a) //.flatten()
    }


    println(bgpileup_nopk)


    var ln_bgpileup_nopk = log(bgpileup_nopk) 
    var ln_bgpileup_pk = log(bgpileup_pk)
    
    //println("ln_bgpileup_nopk")
    //println(ln_bgpileup_nopk)
    //println("ln_bgpileup_pk")
    //println(ln_bgpileup_pk)
    println(ln_bgpileup_nopk.rows)
    println(ln_bgpileup_pk.rows)

    bglognormal_mu      = mean(ln_bgpileup_nopk(::,*)).t
    peaklognormal_mu    = mean(ln_bgpileup_pk(::,*)).t
    bglognormal_sigma   = stddev(ln_bgpileup_nopk(::,*)).t
    peaklognormal_sigma = stddev(ln_bgpileup_pk(::,*)).t

    println("mus")
    println(bglognormal_mu )     
    println(peaklognormal_mu)   
    println("sigmas")
    println(bglognormal_sigma)   
    println(peaklognormal_sigma) 

        
  }
  */

  def loadPriorRegions() = {
      val numpriorsamples = numlntrainpeaks.toDouble/ppeak

      if (regionfn.isDefined) {
        regions = io.Source.fromFile(regionfn.get).getLines.toList.map(
        a =>  (a.split("\\s+")(0), a.split("\\s+")(1).toLong, a.split("\\s+")(2).toLong)   )
      } else {
        regions = rrg.get.generateRandomRegions(numpriorsamples.toInt)
      }
  }

  def saveLognormals(outfn: String) = {
    var ln_outfh = new File( outfn )
    var ln_outfile = new FilePrinter( ln_outfh )

    val labels = Array("background_mu", "background_sigma", "peak_mu", "peak_sigma" )
    val toPrint = Array(bglognormal_mu, bglognormal_sigma, peaklognormal_mu, peaklognormal_sigma)

    Range(0,4).map(i => ln_outfile.println(labels(i) +  "\t" +  toPrint(i).toArray.mkString("\t")) )    
    ln_outfile.closeBufferedWriter()
  }

  def fitLognormals(lognormalParamsFn:String) = {
    
    println("fitting lognormals from file")
    
    val data = io.Source.fromFile(lognormalParamsFn).getLines.toList.map(
                              line => DenseVector[Double](  line.split("\t").toArray.drop(1).map(a => a.trim.toDouble) ))
    
    println(data)
                            
    bglognormal_mu = data(0)
    bglognormal_sigma = data(1)
    peaklognormal_mu = data(2)
    peaklognormal_sigma = data(3)
  }

  def fitLognormals() = {
 
    println("fitting lognormals from scratch")

    var pileupMaps:List[Map[Double,Long]] = sampleinfo.map(a => Map[Double,Long]())

    for(ind <- 0 to regions.length - 1) {
      //test
      val r = regions(ind)
      val pileups = mfr.getRegion(r._1,r._2,r._3)._1.map(t => t.pileup)
      //for (sampleind <- 0 to sampleinfo.length - 1){
        //val pups = pileups(sampleind)

      pileupMaps = pileupMaps.zipWithIndex.map(
          a => a._1 + ( pileups(a._2) -> (if (a._1.contains(pileups(a._2))) a._1(pileups(a._2)) + 1.toLong  else 1.toLong)))
      //}
    }

    //then create the mean/variance for peaks once that is done
    var priorpkcount:Int = scala.math.ceil(ppeak * regions.length).toInt
    var priornopkcount:Int = scala.math.floor((1.0-ppeak) * regions.length).toInt

    //loop through the keys of each sample map in order of size
    var _bglognormal_mu      = DenseVector.zeros[Double](sampleinfo.length)
    var _peaklognormal_mu    = DenseVector.zeros[Double](sampleinfo.length)

    var _bglognormal_sq_mu   = DenseVector.zeros[Double](sampleinfo.length)
    var _peaklognormal_sq_mu = DenseVector.zeros[Double](sampleinfo.length)

    var _bglognormal_sigma   = DenseVector.zeros[Double](sampleinfo.length)
    var _peaklognormal_sigma = DenseVector.zeros[Double](sampleinfo.length)


    for (sampleind <- 0 to sampleinfo.length - 1){
      //sort in descending order by key
      val pileup_amounts = pileupMaps(sampleind).toSeq.sortWith(_._1 > _._1).map(b => b._1).toList
      var running_count:Long = 0.toLong

      for(ind <- 0 to pileup_amounts.length - 1) {

        val ln_pileup = log(pileup_amounts(ind)) 
        var count = pileupMaps(sampleind)(pileup_amounts(ind))
        //now figure out where to add the count to

        var countforpks = 0.toLong
        var countforbg = 0.toLong

        if (running_count > priorpkcount) {
          //pileups now into bgpeaks
          countforbg = count
        } else if (running_count + count <= priorpkcount ) {
          //all pileups still in peaks
          countforpks = count
        } else {
          //some pileups in peaks, some in not peaks
          countforpks = (priorpkcount - running_count)
          countforbg = count - countforpks 
        }

        _bglognormal_mu(sampleind)  = _bglognormal_mu(sampleind) + countforbg.toDouble*ln_pileup/priornopkcount.toDouble
        _bglognormal_sq_mu(sampleind)  = 
        _bglognormal_sq_mu(sampleind) + countforbg.toDouble*ln_pileup*ln_pileup/priornopkcount.toDouble

        _peaklognormal_mu(sampleind)  = _peaklognormal_mu(sampleind) + countforpks.toDouble*ln_pileup/priorpkcount.toDouble
        _peaklognormal_sq_mu(sampleind)  = 
        _peaklognormal_sq_mu(sampleind) + countforpks.toDouble*ln_pileup*ln_pileup/priorpkcount.toDouble


        running_count = running_count + count
      }

    }

    _bglognormal_sigma   = sqrt(_bglognormal_sq_mu - _bglognormal_mu*_bglognormal_mu)
    _peaklognormal_sigma = sqrt(_peaklognormal_sq_mu - _peaklognormal_mu*_peaklognormal_mu)

    bglognormal_mu = _bglognormal_mu       
    bglognormal_sigma = _bglognormal_sigma    
    peaklognormal_mu = _peaklognormal_mu    
    peaklognormal_sigma = _peaklognormal_sigma
       
  }


  def getPriorRegions():List[(String,Long,Long)] = {
    return regions
  }


  //store the maximum likelihood lognormal distributions - for peak and no peak
}
















/*
class SimpleCallerIntermediateCalculationStore(sampleinfo:List[List[String]],ppeak:Double,
          mfr:MultiFileReader,rrg:RandomRegionGenerator ) extends IntermediateCalculationStore(sampleinfo, -1, "" , "", ppeak){

  override def loadStore() = {
    loadSharingMatrix()
    loadLognormals()
  }

  override def loadLognormals() = {
  
      val numpriorsamples = 100.0/ppeak
      val regions = rrg.generateRandomRegions(numpriorsamples.toInt)

      var pileupMaps:List[Map[Double,Long]] = sampleinfo.map(a => Map[Double,Long]())

      for(ind <- 0 to regions.length - 1) {
        //test
        val r = regions(ind)
        val pileups = mfr.getRegion(r._1,r._2,r._3)._1.map(t => t.pileup)
        for (sampleind <- 0 to sampleinfo.length - 1){
          val pups = pileups(sampleind)

          pileupMaps = pileupMaps.map(
            a => a + ( pups -> (if (a.contains(pups)) a(pups) + 1.toLong  else 1.toLong)))
        }
      }

      //then create the mean/variance for peaks once that is done
      var priorpkcount:Int = scala.math.ceil(ppeak * regions.length).toInt
      var priornopkcount:Int = scala.math.floor((1.0-ppeak) * regions.length).toInt
    
      //loop through the keys of each sample map in order of size
      var bglognormal_mu      = DenseVector.zeros[Double](sampleinfo.length)
      var peaklognormal_mu    = DenseVector.zeros[Double](sampleinfo.length)

      var bglognormal_sq_mu   = DenseVector.zeros[Double](sampleinfo.length)
      var peaklognormal_sq_mu = DenseVector.zeros[Double](sampleinfo.length)
      
      var bglognormal_sigma   = DenseVector.zeros[Double](sampleinfo.length)
      var peaklognormal_sigma = DenseVector.zeros[Double](sampleinfo.length)
     

      for (sampleind <- 0 to sampleinfo.length - 1){
        //sort in descending order by key
        val pileup_amounts = pileupMaps(sampleind).toSeq.sortWith(_._1 > _._1).map(b => b._1).toList
        var running_count:Long = 0.toLong

        for(ind <- 0 to pileup_amounts.length - 1) {
       
          val ln_pileup = log(pileup_amounts(ind)) 
          var count = pileupMaps(sampleind)(pileup_amounts(ind))
          //now figure out where to add the count to
          
          var countforpks = 0.toLong
          var countforbg = 0.toLong

          if (running_count > priorpkcount) {
            //pileups now into bgpeaks
            countforbg = count
          } else if (running_count + count <= priorpkcount ) {
            //all pileups still in peaks
            countforpks = count
          } else {
            //some pileups in peaks, some in not peaks
            countforpks = (priorpkcount - running_count)
            countforbg = count - countforpks 
          }

          bglognormal_mu(sampleind)  = bglognormal_mu(sampleind) + countforbg.toDouble*ln_pileup/regions.length.toDouble
          bglognormal_sq_mu(sampleind)  = 
              bglognormal_sq_mu(sampleind) + countforbg.toDouble*ln_pileup*ln_pileup/regions.length.toDouble

          peaklognormal_mu(sampleind)  = peaklognormal_mu(sampleind) + countforpks.toDouble*ln_pileup/regions.length.toDouble
          peaklognormal_sq_mu(sampleind)  = 
              peaklognormal_sq_mu(sampleind) + countforpks.toDouble*ln_pileup*ln_pileup/regions.length.toDouble


          running_count = running_count + count
        }

      }
            

       
  }


}
*/






class BaseCaller(ics:IntermediateCalculationStore, statsfh:Option[FilePrinter]) {

  printStatsHeader()

  def printStatsHeader() = {
    if (statsfh.isDefined) {
      var start = "region\t"
      if (ics.sharingind != -1){start = "region\tbeta\tsd_beta\t"}
      statsfh.get.println(start + ics.sampleids.toArray.mkString("\t") )
    }
  }

  def printStats(regionstr:String, peak:DenseVector[Double],extra:String = "") = {
    //println(peak)
    var outextra = extra
    if (statsfh.isDefined) {
      if (outextra != "") { outextra = "\t" + outextra } 
      statsfh.get.println(regionstr + outextra + "\t" + peak.toArray.mkString("\t") )
    }
  }

  def lognormalpdf(pileupvec:DenseVector[Double], forpeak:Boolean):DenseVector[Double]  = {
    
    var mu = ics.bglognormal_mu
    var sigma = ics.bglognormal_sigma
    if (forpeak) {
      mu = ics.peaklognormal_mu
      sigma = ics.peaklognormal_sigma   
    }
    //println("mu")
    //println(mu)
    //println("sigma")
    //println(sigma)
    var pdf_num =    exp(  (log(pileupvec) - mu) *:* (log(pileupvec) - mu) *:* ( 1.0/(  sigma *:* sigma * 2.0 ) ) * -1.0 )
    //println("pdf_num")
    //println(pdf_num)
    var returnval =  pdf_num *:* (  1.0 / ( pileupvec *:* sigma * sqrt( 2.0 * Pi) )  )
    return returnval
  }

  def lognormalunivpdf(pileup:Double, forpeak:Boolean, x_index:Int):Double = {
    var mu = ics.bglognormal_mu(x_index)
    var sigma = ics.bglognormal_sigma(x_index)
    if (forpeak) {
      mu = ics.peaklognormal_mu(x_index)
      sigma = ics.peaklognormal_sigma(x_index)   
    }
    var pdf_num =    exp(  (log(pileup) - mu) * (log(pileup) - mu) * ( 1.0/(  sigma * sigma * 2.0 ) ) * -1.0 )
    return pdf_num * (  1.0 / ( pileup * sigma * sqrt( 2.0 * Pi) )  )
  }


  def isingcondposteriorxipdf(
    x:DenseVector[Double],x_index:Int, pileups:List[SamplePileups] ,sampleisingonly:Boolean, beta:Double) = {
    
    
    var saved_x_i = x(x_index)
    x(x_index) = 1.0
    
    var p_1_y_given_x = 1.0
    var p_0_y_given_x = 1.0
    if ( ! sampleisingonly ) {
      p_1_y_given_x = (lognormalunivpdf(pileups(x_index).pileup.toDouble, true, x_index)  )
      p_0_y_given_x = lognormalunivpdf(pileups(x_index).pileup.toDouble, false, x_index)
    }
    
    var p_1 = exp(ics.edgeMatrix(x_index,::)*x + (ics.sharingMatrix(x_index,::) *:* beta )*x  )*p_1_y_given_x
    var p_0 = p_0_y_given_x

    var new_x_i = 1.0
    var prob_1 = p_1 / (p_0 + p_1)
    val uniform = new Uniform(0.0, 1.0)
    if (uniform.draw() > prob_1) {
      new_x_i = 0.0
    }
    x(x_index) = new_x_i

  }


  def gibbsIteration(x:DenseVector[Double],pileups:List[SamplePileups],sampleisingonly:Boolean=true, beta:Double=0.0  ) = {
 
    for(x_index <- 0 to x.length - 1) {
      isingcondposteriorxipdf(x,x_index, pileups, sampleisingonly, beta) 
    }
  }

  def p_y_given_x(x:DenseVector[Double],pileups:List[SamplePileups]):Double = {
    var output:Double = 0.0
    for(x_index <- 0 to x.length - 1) {
      //I log the output to avoid underrun errors
      output = output + log(lognormalunivpdf(pileups(x_index).pileup.toDouble, ( x(x_index) == 0.0  ), x_index) )
    }
    return exp(output)
  }

}



class SimpleCaller(ics:IntermediateCalculationStore, statsfh:Option[FilePrinter]) extends BaseCaller(ics, statsfh)  {

  def runcaller(pileups:List[SamplePileups], regionstr:String ):DenseVector[Double]  = {
    var pileupvec = DenseVector(pileups.map(a => a.pileup.toDouble).toArray)
    var peakprobs = lognormalpdf(pileupvec, true)*ics.peakprob
    var bgprobs = lognormalpdf(pileupvec, false)*(1.0-ics.peakprob)
    
    var output =  peakprobs *:* (1.0/(peakprobs + bgprobs))
    
    if (sum(output).isNaN || sum(output).isInfinite) {
      println("NaN found!!!")
      println(pileupvec)
      println(peakprobs)
      println(bgprobs)
      println(output)

      //nasty hack just to get the ising fitter to finish
      output = DenseVector( 
        Range(0,pileups.length).map(
        a => if (output(a).isNaN) 
        (if (pileupvec(a) > ics.peaklognormal_mu(a)) (1.0.toDouble, a ) else (0.0.toDouble, a )  ) else 
         (output(a).toDouble, a ) ).map(
        b => if (output(b._2).isInfinite)
        (if (peakprobs(b._2) > bgprobs(b._2)) 1.0.toDouble else 0.0.toDouble ) else b._1 ).toArray 
      )
    }

    printStats(regionstr,output)
    return output
  }

}


class IsingLognormalCaller(ics:IntermediateCalculationStore,iterations:Int, burnin:Int, sampleevery: Int, statsfh:Option[FilePrinter]) extends BaseCaller(ics, statsfh) {


  def runcaller(pileups:List[SamplePileups], regionstr:String):DenseVector[Double] = {
    var sumpeaks = DenseVector.zeros[Double](pileups.length)
    var counter = 0
    var x = DenseVector.zeros[Double](pileups.length)
    for(k <- 1 to iterations) {
      gibbsIteration(x, pileups, false)
      if (k > burnin && k % sampleevery == 0) {
        sumpeaks = sumpeaks + x
        counter = counter + 1
      }
    }
    //println(sumpeaks/counter.toDouble)
    printStats(regionstr,sumpeaks/counter.toDouble)
    return sumpeaks/counter.toDouble
  }




}






class IsingLognormalSharingCaller(ics:IntermediateCalculationStore,iterations:Int, burnin:Int, sampleevery: Int, statsfh:Option[FilePrinter]) extends BaseCaller(ics, statsfh) {



  def foldednormalpdf(x:Double, mu:Double, sigma:Double):Double = {
    //get the sd for the folded normal from the ics
    if (x < 0){ return 0.0 }
    var pdf_num1 =    exp(  (x - mu) * (x - mu ) * ( 1.0/(  sigma * sigma * 2.0 ) ) * -1.0 )
    var pdf_num2 =    exp(  (x + mu) * (x + mu ) * ( 1.0/(  sigma * sigma * 2.0 ) ) * -1.0 )
    return (pdf_num1 + pdf_num2   ) * (  1.0 / ( sigma * sqrt( 2.0 * Pi) )  )
  }

  def foldednormalsample(mu:Double, sigma:Double):Double = {
    val g = breeze.stats.distributions.Gaussian(mu,sigma)
    return abs(g.draw) 
  }



  def samplemarginalposterior(pileups:List[SamplePileups], proposal_beta:Double, prior_sigma:Double):Double = {

    var s_iterations:Int = 40
    var s_burnin:Int = 10
    var s_sampleevery:Int = 5

    var samplesforbeta:DenseMatrix[Double] = 
    DenseMatrix.zeros[Double](pileups.length, s_iterations/s_sampleevery - s_burnin/s_sampleevery )
    println("exactapproxstoresize:" + (s_iterations/s_sampleevery - s_burnin/s_sampleevery).toString )

    var x_for_beta = DenseVector.zeros[Double](pileups.length)
    var mhcounter = 0 
    for(j <- 1 to 40) {
      gibbsIteration(x_for_beta, pileups, true, proposal_beta )
      if (j > 10 && j % 5 == 0) {
        samplesforbeta(::,mhcounter) := x_for_beta
        mhcounter = mhcounter + 1
      }
    }
    println("mhcounter" + mhcounter.toString )
    //ok got the sample for p(x|beta), next calcuate approx. marginal likelihood p(y|beta)        
    var sum_y_given_x = 0.0
    for(p <- 0 to samplesforbeta.cols - 1) {
      sum_y_given_x = sum_y_given_x + p_y_given_x(samplesforbeta(::,p), pileups)
    }
    var marg_lik = sum_y_given_x / (samplesforbeta.cols.toDouble) 
    var marg_posterior_beta = marg_lik * foldednormalpdf(proposal_beta,0.0,prior_sigma )
    return marg_posterior_beta
  }

  //returns the call as well as the sharing and the 95% confidence interval for sharing
  def runcaller(pileups:List[SamplePileups], regionstr:String):(DenseVector[Double], (Double, Double)  ) = {


    val uniform = new Uniform(0.0, 1.0)
    var prior_sigma = stddev(abs(ics.edgeMatrix))

    var counter = 0
    var sumpeaks = DenseVector.zeros[Double](pileups.length)
    var betastore = DenseVector.zeros[Double](iterations/sampleevery - burnin/sampleevery)
    println("storesize:" + (iterations/sampleevery - burnin/sampleevery).toString )
    var acceptprobstore:List[Double] = List()
 

    var x = DenseVector.zeros[Double](pileups.length)

    var beta:Double = 0.0
    var old_beta:Double = 0.0
    var old_marg_posterior_beta:Double = 0.0
    var marg_posterior_beta:Double = samplemarginalposterior(pileups,beta, prior_sigma)


    for(k <- 1 to iterations) {
        
      //this is a collapsed gibbs sampler
      //first get a beta sample - using exact approximate inference M-H 
        //sample beta from the proposal
        //start by getting a sample of the likelihood
        //calcuate the prior
        //decide on new beta
      //After new beta, run the
      old_beta = beta
      old_marg_posterior_beta = marg_posterior_beta
      var proposal_beta = foldednormalsample(old_beta, prior_sigma)
      var proposal_marg_posterior_beta = samplemarginalposterior(pileups,proposal_beta, prior_sigma) 

      //do the M-H
      var MH_ratio = (proposal_marg_posterior_beta * foldednormalpdf(old_beta, proposal_beta, prior_sigma)  ) /
                     (old_marg_posterior_beta * foldednormalpdf(proposal_beta, old_beta, prior_sigma)   )
      //decide to accept/reject based on the ratio
      if (MH_ratio >= 1.0 || uniform.draw <= MH_ratio ) {
        beta = proposal_beta
        marg_posterior_beta = proposal_marg_posterior_beta
      }
      acceptprobstore = acceptprobstore ++ List( scala.math.min(1.0, MH_ratio) )

     
      //now sample x using M-H value of beta from p(beta|y)
      //i.e. sample from p(x|y,beta)
      gibbsIteration(x, pileups,false, beta)

      if (k > burnin && k % sampleevery == 0) {
        sumpeaks = sumpeaks + x
        //add beta to list of betas
        betastore(counter) = beta
        counter = counter + 1
      }
    }

    println("counter" + counter.toString )
    var output = (sumpeaks/counter.toDouble, ( mean(betastore) , stddev(betastore)    ) )
    println( output._1 ) 
    println( output._2._1.toString + " " +  output._2._2.toString )
    println( acceptprobstore  )
    printStats(regionstr, output._1, output._2._1.toString + "\t" + output._2._2.toString )
    return output

    


  }





}







class RandomRegionGenerator(windowSize:Long, chroms:List[(String, Long, Long)]) {

  def generateRandomRegions(numregions:Int):List[(String,Long,Long)] = {
    //do for number of windows
      //choose a chromosome
      //get its size 
      //select a random number
      //generate the window
    var output:List[(String, Long, Long)] = List()
    var chroms_sizes = DenseVector(chroms.map(j => (j._3 - j._2 ).toDouble ).toArray)
    chroms_sizes = chroms_sizes/chroms_sizes.sum
    var mnomial = new Multinomial(chroms_sizes)

    var a = 0
    for( a <- 0 to numregions - 1){
      var chrom = chroms(mnomial.draw) 
      val randpos =  Random.nextInt(chrom._3.toInt - chrom._2.toInt - windowSize.toInt + 1).toLong + chrom._2
      output = output ++ List( (chrom._1, randpos, randpos + windowSize) )
    }
    return output
  }


}


/*
class PriorSampleSimpleCaller(sampleinfo:List[List[String]],ppeak:Double,mfr:MultiFileReader,
                              rrg:RandomRegionGenerator,numregions:Int ) {
  var caller:SimpleCaller = _
  createPriorSampleSimpleCaller()
   
  def createPriorSampleSimpleCaller() = {
    var scics = new SimpleCallerIntermediateCalculationStore(sampleinfo,ppeak,mfr,rrg )
    var sc = new SimpleCaller(scics, sc_statsfh )
  }

  def runcaller(pileups:List[SamplePileups], regionstr:String ):DenseVector[Double]  = {
    return sc.runcaller(pileups, regionstr)
  }

}
*/

class IsingFitter(ics:IntermediateCalculationStore, statsfh:Option[FilePrinter], 
                  sampleinfo:List[List[String]], mfr:MultiFileReader,
                  edgefn:String, ppeak:Double, callThreshold: Double, useHardCalls: Boolean, 
                  isingsymmetrifyAND: Boolean, rrg:RandomRegionGenerator ) {

  //TODO
  //online learning sequence of L1 regularised logistic regressions - one regression for each sample
  //try minibatch
  //stochastic gradient descent, or gradient descent
  
  //loop
    //read random samples
    //call peaks using the PriorSampleSimpleCaller
    //add to logistic regression params

  //At end of loop, take mean of bjk bkj's - and figure out which are zero

  //return sharing matrix
  def fitEdgeMatrix(numbatches:Int,batchsize:Int,EBIC_samplesize:Int) = {


    //do this in batches
    
    //The params I need to consider
    //1. nu - the learning rate for SGD
    //2. g - the sparsity parameter
    var modelParams:List[(Double,Double)] = List((1.0, 0.01),(1.0, 0.1),(1.0, 1.0),(1.0,5.0),(5.0,1.0),(10.0,5.0),(10.0,10.0))
    var edgeMatrices:MutableList[DenseMatrix[Double]] =  MutableList()  ++
                    Range(0,modelParams.length).map(a => DenseMatrix.zeros[Double](sampleinfo.length,sampleinfo.length) ).toList
    val sc = new SimpleCaller(ics, statsfh)
    
    //var numbatches = 200
    //var batchsize = 10000
    //var EBIC_samplesize = 1000000

    for(batch <- 0 to numbatches - 1) {

      println("ising fitter starting batch: "+batch)
      val regions = rrg.generateRandomRegions(batchsize)

      for(ind <- 0 to regions.length - 1) {
        //test
        val r = regions(ind)
        val pileups = mfr.getRegion(r._1,r._2,r._3)._1 //.map(t => t.pileup)
        val regionstr = r._1 + ":" + r._2.toString + "-" +  r._3.toString
        
        //A vector of probs of being a peak 
        val calls_p = sc.runcaller(pileups, regionstr)
        //figure out if to use calls as hard calls or probabilities for logistic regression independent vars
        val hardcalls:DenseVector[Double] = DenseVector[Double]( calls_p.toArray.map(a => if (a >= callThreshold) 1.0 else 0.0 ) )
        val calls = if (useHardCalls) hardcalls else calls_p

        //Next step is to update my weights, so I need an internal edge matrix
        for(modelind <- 0 to edgeMatrices.length - 1) {

          //println("updating weights model: "+modelind)

          val nu = modelParams(modelind)._1 * ( 1.0/sqrt(((batch+1)).toDouble) )
          val g = modelParams(modelind)._2
          val A = hardcalls * calls.t
          val E = edgeMatrices(modelind)

          val sgd_term1 =   
          tile( 1.0/(DenseVector.ones[Double](E.rows) + exp( -1.0*(E*calls - diag(diag(E))*calls + diag(E)) ) )  , 1, E.rows ) *:*
          (tile(calls.t,1,E.rows) - diag(calls) + DenseMatrix.eye[Double](E.rows)     )
          
          val sgd_term2 = ( A - diag(diag(A)) + diag(hardcalls) )

          edgeMatrices(modelind) = E - nu*( sgd_term1 - sgd_term2 )
      

          //println("=== stats ===")
          //println(calls_p)
          //println(nu)
          //println(g)
          //println(sgd_term1.toArray.mkString("\t"))
          //println(sgd_term2.toArray.mkString("\t"))
          //println()

          if (ind == regions.length - 1) {

            println("sparsifying model: "+modelind)

            var E_tosp = edgeMatrices(modelind)
            //edgeMatrices(modelind) = sparsify(E_tosp-diag(diag(E_tosp)),g*nu*batchsize.toDouble) + diag(diag(E_tosp))
            edgeMatrices(modelind) = sparsify(E_tosp-diag(diag(E_tosp)),g*nu) + diag(diag(E_tosp))
          
            println((edgeMatrices(modelind) - diag(diag(edgeMatrices(modelind)))).toArray.mkString("\t") )
            
            println()
            println()

          }
        }

      }

    }


    println("Going into EBIC stage")


    //run the EBIC - to choose the correct sparsity setting/learning rate
    //Need to run EBIC for each regression separately (i.e. each row of E), and then choose the model weights with the best result
    val gamma = 0.25
    val ebicregions = rrg.generateRandomRegions(EBIC_samplesize)
    var winningModelE:DenseMatrix[Double] = DenseMatrix.zeros[Double](sampleinfo.length,sampleinfo.length)


    var EBICneighbours:MutableList[DenseVector[Double]] = MutableList()  ++
      Range(0,modelParams.length).map(a => 
        I(edgeMatrices(a)  - diag(diag(edgeMatrices(a))))*DenseVector.ones[Double](sampleinfo.length)
      ).toList
    var EBICs:MutableList[DenseVector[Double]] = MutableList()  ++
      Range(0,modelParams.length).map(
        a => log(ebicregions.length.toDouble)*EBICneighbours(a) + 
                 2.0*gamma*EBICneighbours(a)*log(sampleinfo.length.toDouble -1.0)   ).toList

    println("prepared EBIC variables")


    for(ind <- 0 to ebicregions.length - 1) {
      val r = ebicregions(ind)
      val pileups = mfr.getRegion(r._1,r._2,r._3)._1 //.map(t => t.pileup)
      val regionstr = r._1 + ":" + r._2.toString + "-" +  r._3.toString
      val calls_p = sc.runcaller(pileups, regionstr)
      val hardcalls:DenseVector[Double] = DenseVector[Double]( calls_p.toArray.map(a => if (a >= callThreshold) 1.0 else 0.0 ) )
      val calls = if (useHardCalls) hardcalls else calls_p
      for(modelind <- 0 to edgeMatrices.length - 1) {
        //just add -2.0*log(likelihood) to the EBICs as per EBIC eqn.
        val E = edgeMatrices(modelind)
        //figure out if to use calls as hard calls or probabilities for logistic regression independent vars
        val E_x_calls = edgeMatrices(modelind)*calls - diag(diag(E))*calls + diag(E)
        val ll_ofpoint = E_x_calls *:* hardcalls - log(DenseVector.ones[Double](E.rows) + exp(E_x_calls))
        EBICs(modelind) = EBICs(modelind) - 2.0*ll_ofpoint

      }
    }

    //Then for each row of the final E matrix, choose the row with the lowest EBIC
    println( "___ISING EBIC model selection___" )
    println("=== All EBICS ===")
    EBICs.map(a => println(a))
    println("=================")
    println("")
    println("=== Row by Row ===")

    for(ind <- 0 to winningModelE.rows - 1) {
      var winningModel = argmin(DenseVector( EBICs.map(a => a(ind)).toArray  ))

      println( "row: "+ind+" winning model: "+modelParams(winningModel)._1 + " " + modelParams(winningModel)._2  )

      winningModelE(ind,::) := edgeMatrices(winningModel)(ind,::)  
    }

    println("===============")

    //Lastly symmetrify the matrices 
    for(i <- 0 to winningModelE.rows - 1) {
      for(j <- 0 to i-1) {
        //compare the weight with the opposite weight, and set both accordingly
        if ( (isingsymmetrifyAND &&  winningModelE(i,j) != 0.0 && winningModelE(j,i) != 0.0) || 
             (!isingsymmetrifyAND &&  (winningModelE(i,j) != 0.0 || winningModelE(j,i) != 0.0))){
          val meanweight = (winningModelE(i,j) + winningModelE(j,i))/2.0
          winningModelE(i,j) = meanweight
          winningModelE(j,i) = meanweight
        } 
        else {
          winningModelE(i,j) = 0.0
          winningModelE(j,i) = 0.0
        }
      }
    }

    //And save the matrix
    var winningModelE_outfh = new File( edgefn )
    var winningModelE_outfile = new FilePrinter( winningModelE_outfh )
    Range(0,winningModelE.rows).map(i => winningModelE_outfile.println( winningModelE(i,::).t.toArray.mkString("\t")) )
    winningModelE_outfile.closeBufferedWriter()
  }


}







class BedOutputter(sampleinfo:List[List[String]],sharing_ind: Int, peakcallthreshold:Double, 
                   outputstem:String, merged_outputfn:String) {

  var sampleids:Array[String] = _
  
  var merged_activesids:Set[String] = _

  var merged_peakidcounter = 1.toLong
  var peakidcounters:Array[Long] = _
  
  var merged_peakstore:Array[(String,Long,Long,String,Double,Option[(Double,Double)])] = _
  var peakstore:Array[Array[(String,Long,Long, Double)]] = _

  var bedfiles:Array[FilePrinter] = _
  var merged_bedfile:FilePrinter = _

  var currentchr:String  = ""

  initialise()

  //sample bed file output,
  //chr  start  stop  pk#  pprob*1000  .  pprob  -1  -1 -1  location|sharing_mn|sharing_sd:sid1|ppk,sid2|ppk,sid3|ppk;location:sid1|ppk
  //last column for merged only

  def initialise() = {
    peakidcounters = Array.fill[Long](sampleinfo.length)(1.toLong)
    merged_activesids = Set()
    
    if (sharing_ind != -1 ){
      sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","") + "$" + a(sharing_ind + 1) ).toArray
    } else {
      sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","")).toArray
    }
    bedfiles = sampleids.map(a => createOutputFile(outputstem + "/" + a + ".peakonomic.bed") ).toArray
    merged_bedfile = createOutputFile(outputstem + "/" + merged_outputfn + ".peakonomic.mbed")
    merged_peakstore = Array()
    peakstore = Range(0,sampleinfo.length).map(a => Array[(String,Long,Long, Double)]()).toArray
  }

  def createOutputFile(filepath:String):FilePrinter = {
    var outfh = new File( filepath )
    var outfile  = new FilePrinter(outfh)
    return outfile
  }

  def flushpeaktofile(ind:Int) = {
    val maxprob = peakstore(ind).map(a => a._4).max
    var outstring = peakstore(ind)(0)._1  + "\t" + peakstore(ind)(0)._2.toString + "\t" + peakstore(ind).last._3.toString + "\t" +
                    "pk" + peakidcounters(ind).toString + "\t" + (maxprob*1000).toInt.toString + "\t" + "." + "\t" + 
                    maxprob.toString + "\t-1\t-1\t-1"
    bedfiles(ind).println( outstring ) 
    peakidcounters(ind) = peakidcounters(ind) + 1.toLong
    peakstore(ind) = Array()
  }


  def flushpeaktomergedfile() = {
    
    //chr  start  stop  pk#  pprob*1000  .  pprob  -1  -1 -1  location|sharing_mn|sharing_sd!sid1|ppk,sid2|ppk,sid3|ppk;location...
    val maxprob = merged_peakstore.map(a => a._5).max

    var outstring = merged_peakstore(0)._1  + "\t" + 
                    merged_peakstore(0)._2.toString + "\t" + merged_peakstore.last._3.toString + "\t" +
                    "pk" + merged_peakidcounter.toString + "\t" + (maxprob*1000).toInt.toString + "\t" + "." + "\t" + 
                    maxprob.toString + "\t-1\t-1\t-1\t"

    outstring = outstring + merged_peakstore.map(a => a._1 + ":" + a._2.toString + "-" + a._3.toString + 
                (if (a._6.isDefined) "|" + a._6.get._1.toString + "|" + a._6.get._2.toString  else "|-|-") + "!" +
                a._4).mkString(";")

    merged_bedfile.println(outstring)
    merged_peakidcounter = merged_peakidcounter + 1.toLong
    merged_activesids = Set()
    merged_peakstore = Array()
  }


  def addwindowresults(chr:String, startpos:Long, endpos:Long, peakprobs:Array[Double], beta:Option[(Double,Double)]) = {
    
    //if the chromosome is different and chr != "" - flush all
    //for any samples in prev, but not in this, flush
    //push new samples
    //if intersection with mergedactive is 0,and mergedactive is non-empty, flush merged
    //add merged to list
    
    val probs_w_index  = peakprobs.zipWithIndex
    val peakstore_w_index = peakstore.zipWithIndex
    val sampleswpeak = probs_w_index.map(a => if (peakcallthreshold <= a._1) sampleids(a._2) else "" ).filter(a => a != "").toSet
    
    if (chr != currentchr){
      if (merged_peakstore.length > 0) {flushpeaktomergedfile()}
      for (samp <- peakstore_w_index if samp._1.length > 0 ){ flushpeaktofile(samp._2) }
    }
    else {
      if (merged_peakstore.length > 0 && (merged_peakstore.last._3 != startpos || 
          merged_activesids.intersect(sampleswpeak).size == 0 )) {
        println("flushing peak to merged file")
        flushpeaktomergedfile()
      }
      for (samp <- peakstore_w_index if 
        samp._1.length > 0 && (samp._1.last._3 != startpos || ! (sampleswpeak contains sampleids(samp._2) )   )){
          println("flushing peak to file "+samp._2)
          flushpeaktofile(samp._2) 
        }
    }


    //then add new content
    merged_activesids = merged_activesids union sampleswpeak
    if (sampleswpeak.size > 0) {
      var windowPkString:List[String] = List()
      var probs_for_max:Array[Double] = Array()
      for (samp <- probs_w_index if sampleswpeak contains sampleids(samp._2)) { 
                   windowPkString = windowPkString ++ List( sampleids(samp._2) + "|" + samp._1.toString ) 
                   probs_for_max = probs_for_max :+ samp._1
                 }
      merged_peakstore = merged_peakstore ++ 
                            Array( (chr,startpos,endpos, windowPkString.mkString(","), probs_for_max.max , beta )  )
    }
    for (samp <- peakstore_w_index if sampleswpeak contains sampleids(samp._2)) { 
                            peakstore(samp._2) =  peakstore(samp._2) :+ (chr,startpos,endpos,peakprobs(samp._2)) }
    currentchr = chr



    
  }


  def closeFiles() = {
    //TODO: flush any remaining peaks!!!!
    bedfiles.map(a => a.closeBufferedWriter())
    merged_bedfile.closeBufferedWriter()
  }



}



case class AppArgs(
  mode: Option[String],
  sampleinfofn: Option[String],
  outputstem: Option[String],

  lnfn: Option[String],
  edgematrixfn: Option[String],
  chrs: Option[String],
  chrsfn: Option[String],
  regionfn: Option[String],
  isinghardcalls: Boolean,
  isingsymmetrifyAND: Boolean,
  isingnbatches: Int,
  isingbatchsize: Int,
  isingEBICsamplesize: Int,
  ppeak: Double,
  sharingInd: Int,
  windowSize: Long,
  bgSize: Long,
  readmode: String,
  cuts: Boolean,
  numLnTrainPeaks: Int,
  numRandomRegions: Int,
  isingCallerThresh: Double,
  peakCallThesh: Double,
  debug: Boolean,
  wtrainingregions: Boolean

)
object AppArgs {
  def empty = new AppArgs(None, None, None, None, None, None, None, None,false,false,1000,10000,1000000,0.0001,    
                          -1,300.toLong, 3000.toLong, "ATAC", true, 500, 200000, 0.0, 0.90, false, false)
}


object Peakonomic {

  def processArgs(args: Array[String]):AppArgs = {

    val argsInstance = 
      ( args ++ Array("[buffer]") ).sliding(2, 1).toList.foldLeft(AppArgs.empty) { case (accumArgs, currArgs) => currArgs match {
        
        case Array("--mode", mode) => accumArgs.copy(mode = Some(mode))
        case Array("--in", sampleinfofn) => accumArgs.copy(sampleinfofn = Some(sampleinfofn))
        case Array("--out", outputstem) => accumArgs.copy(outputstem = Some(outputstem))
        
        case Array("--help", _) => accumArgs.copy(mode = Some("help"))
        case Array("--isingsymmetrifyAND", _) => accumArgs.copy(isingsymmetrifyAND = true)
        case Array("--isinghardcalls", _) => accumArgs.copy(isinghardcalls = true)
        case Array("--isingnbatches", isingnbatches) => accumArgs.copy(isingnbatches = isingnbatches.toInt)
        case Array("--isingbatchsize", isingbatchsize) => accumArgs.copy(isingbatchsize = isingbatchsize.toInt)
        case Array("--isingEBICsamplesize", isingEBICsamplesize) => accumArgs.copy(isingEBICsamplesize = isingEBICsamplesize.toInt)

        case Array("--isingin", edgematrixfn) => accumArgs.copy(edgematrixfn = Some(edgematrixfn))
        case Array("--lnin", lnfn) => accumArgs.copy(lnfn = Some(lnfn))
        case Array("--chrs", chrs) => accumArgs.copy(chrs = Some(chrs))
        case Array("--chrsin", chrsfn) => accumArgs.copy(chrsfn = Some(chrsfn))
        case Array("--regionsin", regionfn) => accumArgs.copy(regionfn = Some(regionfn))
        case Array("--lntrainingregionsin", regionfn) => accumArgs.copy(regionfn = Some(regionfn))
        case Array("--sharingindex", sharingInd) => accumArgs.copy(sharingInd = sharingInd.toInt)
        case Array("--windowsize", windowSize) => accumArgs.copy(windowSize = windowSize.toLong)
        case Array("--bgwindowsize", bgSize) => accumArgs.copy(bgSize = bgSize.toLong)
        case Array("--readmode", readmode) => accumArgs.copy(bgSize = readmode.toLong)
        case Array("--use-pileups", _) => accumArgs.copy(cuts = false)
        case Array("--nrandomregions", numRandomRegions) => accumArgs.copy(numRandomRegions = numRandomRegions.toInt)
        case Array("--nlntrainpeaks", numLnTrainPeaks) => accumArgs.copy(numLnTrainPeaks = numLnTrainPeaks.toInt)
        case Array("--icthresh", isingCallerThresh) => accumArgs.copy(isingCallerThresh = isingCallerThresh.toDouble)
        case Array("--pcthresh", peakCallThesh) => accumArgs.copy(peakCallThesh = peakCallThesh.toDouble)
        case Array("--ppeak", ppeak) => accumArgs.copy(ppeak = ppeak.toDouble)
        case Array("--debug", _) => accumArgs.copy(debug = true)
        case Array("--wtrainingregions", _) => accumArgs.copy(wtrainingregions = true)
        case unknownArg => accumArgs // Do whatever you want for this case
      }
    }
    return argsInstance
  }

  def printUsage() = {

    val usage ="""
                  |
                  |peakonomic --mode {mode} --in {infile} --out {outfile/folder} {mode parameters} [Options]
                  |
                  |--in {infile}
                  |  The input file describing the sample data
                  |  Format:
                  |    /path/to/bamfile/sample1.bam
                  |    /path/to/bamfile/sample2.bam
                  |    /path/to/bamfile/sample3.bam
                  |    etc.
                  |
                  |--out {outfile/folder}
                  |  The output file or folder. For caller mode
                  |  specify a folder, for other modes an output file
                  |
                  |
                  |Help:
                  |=====
                  |
                  |peakonomic --help
                  |To show this page
                  |
                  |
                  |Modes and their paramaters:
                  |===========================
                  |
                  |--mode lntrain 
                  |  Fits the lognormal distributions for a peak and not peak.
                  |  Used as a simple single sample caller.
                  |  Outputs the lognormal parameters to use in later steps
                  |
                  |  [Required]
                  |
                  |  --[chrs|chrsin] {chrs|chrsfile}
                  |    The genomic regions to use.
                  |    Can specify on the command line with --chrs, or use a file with --chrsin
                  |    Must match the chromosome sequence names in the bam file. 
                  |    e.g. --chrs 1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23
                  |    e.g. --chrs chr1,chr2
                  |    e.g. --chrs chr1:100-5000000,chr2:300000-1000000
                  |    e.g. --chrsin file
                  |         where file has the tab separated format
                  |         chromosome{tab}startpos{tab}endpos
                  |   Positions are as bed file format, zero-based, end exclusive
                  |   (i.e. 0-100, is first base to 100th base inclusive)
                  |
                  |  [Options]
                  |
                  |  --nlntrainpeaks {500}
                  |    How many peaks should be present in the training sample.
                  |    This along with --ppeak determines how many random regions
                  |    are sampled to train the lognormal single sample caller,
                  |    i.e. npeaks/ppeak training samples are generated.
                  |
                  |  --lntrainingregionsin {regionsfile}
                  |    A file of genomic regions to use to train the lognormal
                  |    distributions. Useful for testing. If this is specified, 
                  |    nlntrainpeaks is ignored.
                  |    The format is the same as the chrsin file.
                  |
                  |  --wtrainingregions
                  |    Outputs the regions used to train the lognormal distributions
                  |    Useful for testing.
                  |
                  |
                  |--mode isingtrain
                  |  Fits the pairwise dependency matrix between samples.
                  |  Used by the ising model prior by the multi-sample caller
                  |
                  |  [Required]
                  |
                  |  --lnin {lnfile}
                  |    Path to file with lognormal parameters, the output of
                  |    lntrain
                  |
                  |  --[chrs|chrsin] {chrs|chrsfile}
                  |    The genomic regions to use. As with --mode lntrain
                  |
                  |  [Options]
                  |
                  |  --pcthresh {0.80}
                  |    The probability threshold at which a peak is called
                  |
                  |  --isingbatchsize {10000}
                  |    The number of genomic regions to sample while training
                  |    the ising model. Gradient descent is applied after every
                  |    sampled region, and at the end of the batch, L1 Truncation
                  |    is performed.
                  |
                  |  --isingnbatches {1000}
                  |    The number of batches, i.e. the number of times L1
                  |    Truncation is performed.
                  |
                  |  --isingEBICsamplesize {1000000}
                  |    Test dataset sample size.
                  |    Internally Peakonomic trains multiple Ising models
                  |    with different values of gradient descent and L1 sparsity.
                  |    To choose the best model, the EBIC
                  |    (Extended Bayesian Information Criterion) is calculated
                  |    for each model, and the best model is selected for each 
                  |    logistic regression step in the Ising fitter.
                  |
                  |  --isingsymmetrifyAND
                  |    When symmetrifying the matrix after training,
                  |    an entry is non-zero if both (x,y) and (y,x) entries 
                  |    are non-zero, as opposed to just one.
                  |
                  |  --isinghardcalls
                  |    When training, use hard peak calls when doing 
                  |    logistic regression for one sample against the others.
                  |
                  |
                  |--mode caller
                  |  Runs the peakonomic multi-sample caller.
                  |
                  |  [Required]
                  |
                  |  --[chrs|chrsin] {chrs|chrsfile}
                  |    The genomic regions to use. As with --mode lntrain
                  |
                  |  --lnin {lnfile}
                  |    Path to file with lognormal parameters, the output of
                  |    lntrain
                  |
                  |  --isingin {lnfile}
                  |    Path to file with ising parameters, the output of
                  |    isingtrain
                  |
                  |  [Options]
                  |
                  |  --pcthresh {0.80}
                  |    The probability threshold at which a peak is called
                  |
                  |  
                  |  --icthresh {0}
                  |    Designed to save time, do not use right now!!
                  |
                  |
                  |
                  |--mode randomregions
                  |  Generates a file of random genomic regions of width --windowsize
                  |  within the regions specified by --chrs. Useful for testing.
                  |
                  |  [Required]
                  |
                  |  --[chrs|chrsin] {chrs|chrsfile}
                  |    The genomic regions to use. As with --mode lntrain
                  |
                  |  [Options]
                  |
                  |  --nrandomregions {20000}
                  |    The number of random regions to generate
                  |
                  |
                  |--mode density
                  |  Generates a file of relative read densities for the regions in 
                  |  the file specified by regionsin. Useful for testing.
                  |
                  |  [Required]
                  |
                  |  --regionsin {regionfile}
                  |    The file with genomic regions to use, same format as chrsin file
                  |
                  |
                  |General Options:
                  |================
                  |
                  |Make sure you set these the same across modes
                  |
                  |--ppeak {0.0001}
                  |  Genomic prior probability of a peak. This is an important parameter, 
                  |  and should be experimented with.
                  |
                  |--windowsize {300}
                  |  Move along the genome calling sliding windows of this size
                  |
                  |--bgwindowsize {3000}
                  |  Size of the background window, to calculate relative read density.
                  |  Relative Density is calculated as: (#fgreads/fgsize+1)/(#bgreads/bgsize +1) 
                  |  Background window and foreground window are centered together.
                  |
                  |--readmode {ATAC}
                  |  String, if set to anything other than ATAC, then reads are not offset 
                  |  to account for centre of transposon binding event.
                  |
                  |--use-pileups
                  |  To make Peakonomic consider the number of reads overlapping a window
                  |  instead of the default, which considers cuts in a window.
                  |
                  |--debug 
                  |  Run peakonomic in debug mode, prints helpful .statsout files
                  |  For isingtrain mode:
                  |    .isingpriorcaller.statsout, posteriors from single sample caller
                  |  For caller mode:
                  |    simplecaller.statsout, posteriors from single sample caller
                  |    isinglncaller.statsout, posteriors from multi-sample caller
                  |    compdensities.statsout, comparative read densities
                  |
                  |
                  |*****************************************************
                  |* See https://peakonomic.bitbucket.io for more info *
                  |*****************************************************
                  |
                  |""".stripMargin

    println(usage)
  }

  def main(args: Array[String]) =  {

    val argArr = processArgs(args)
    if (argArr.mode.isEmpty || argArr.mode.get == "help" || argArr.mode.get == "caller" && argArr.edgematrixfn.isEmpty  || 
        argArr.mode.get == "density" && argArr.regionfn.isEmpty ||
        argArr.sampleinfofn.isEmpty || argArr.outputstem.isEmpty || 
        argArr.mode.get != "density" && argArr.chrs.isEmpty && argArr.chrsfn.isEmpty ) {
      printUsage()
      System.exit(0)
    }

    var isingnbatches = argArr.isingnbatches
    var isingbatchsize = argArr.isingbatchsize
    var isingEBICsamplesize = argArr.isingEBICsamplesize

    var isinghardcalls = argArr.isinghardcalls
    var sharing_ind = argArr.sharingInd
    val windowSize:Long = argArr.windowSize
    val bgSize:Long = argArr.bgSize
    val readmode:String = argArr.readmode
    val cuts:Boolean = argArr.cuts
    val numrandomregions = argArr.numRandomRegions
    val numlntrainpeaks = argArr.numLnTrainPeaks
    val ppeak = argArr.ppeak
    val isingcallerthresh = argArr.isingCallerThresh
    val peakcallthreshold = argArr.peakCallThesh
    val debug = argArr.debug
    val wtrainingregions = argArr.wtrainingregions
    val isingsymmetrifyAND = argArr.isingsymmetrifyAND

    val mode = argArr.mode.get
    val sampleinfofn = argArr.sampleinfofn.get 
    val outputstem = argArr.outputstem.get
    val callableregions = argArr.chrs
    val regionfn = argArr.regionfn
    val chrsfn  = argArr.chrsfn
    val merged_outputfn =  "merged"
    val edgematrixfn = argArr.edgematrixfn
    val lnfn = argArr.lnfn


    var chrs:List[(String, Long, Long)] = List()
    if (mode != "density") {
      chrs = if (argArr.chrsfn.isEmpty) callableregions.get.split(",\\s+").toList.map(a => 
        ( a.split("\\:")(0), 
          if(a.split("\\:").length > 1 && a.split("\\:")(1).split("-")(0).trim.length > 0) 
            a.split("\\:")(1).split("-")(0).trim.toLong  else 0.toLong  , 
          if(a.split("\\:").length > 1 && a.split("\\:")(1).split("-")(1).trim.length > 0) 
            a.split("\\:")(1).split("-")(1).trim.toLong  else -1.toLong )  ) else 
        io.Source.fromFile(chrsfn.get).getLines.toList.map(
                              a =>  (a.split("\\s+")(0), a.split("\\s+")(1).toLong, a.split("\\s+")(2).toLong)   ) 
    }


    val sampleinfo = io.Source.fromFile(sampleinfofn).getLines.toList.map(line => line.split("\t").toList.map(a => a.trim))
    sampleinfo.map(a => println(a.mkString(",")))
      
    var priormfr = new  MultiFileReader(None,None,None,sampleinfo,windowSize, bgSize, readmode, cuts)
    chrs = chrs.map(a => (a._1, a._2, if(a._3 == -1.toLong) priormfr.getSequenceLength(a._1)  else a._3 ) )
    var rrg = new RandomRegionGenerator(windowSize, chrs)     
    var ics = new IntermediateCalculationStore(sampleinfo, sharing_ind, priormfr, edgematrixfn, 
                                               ppeak, numlntrainpeaks, Some(rrg), regionfn ) 


    //run the IsingFitter here
    if (mode == "isingtrain"){

      var isingprior_statsfh:Option[FilePrinter] = None
      if (debug){
        var isingprior_statsjavafh = new File( outputstem + ".isingpriorcaller.statsout" )
        isingprior_statsfh  = Some( new FilePrinter(isingprior_statsjavafh)  )
      }

      if (lnfn.isDefined) ics.fitLognormals(lnfn.get)  else  ics.fitLognormals()
      var ipf  = new IsingFitter(ics, isingprior_statsfh, 
                                 sampleinfo, priormfr,outputstem + ".ising", ppeak, peakcallthreshold, isinghardcalls, 
                                 isingsymmetrifyAND,rrg )
      ipf.fitEdgeMatrix(isingnbatches,isingbatchsize,isingEBICsamplesize)
      System.exit(0)

    }

    if (mode == "lntrain"){
      ics.fitLognormals()
      ics.saveLognormals(outputstem + ".lnparams")
      if (wtrainingregions) {
        var regions = ics.getPriorRegions()
        var outfh = new File( outputstem + ".lntrainregions" )
        var outfile  = new FilePrinter(outfh)
        val priorsample = regions.map(r => outfile.println(r._1 + "\t" + r._2.toString + "\t" + r._3.toString ))
        outfile.closeBufferedWriter()
      }      
      System.exit(0)
    }

    if (mode == "randomregions"){
      
      var regions = rrg.generateRandomRegions(numrandomregions.toInt) // ics.getPriorRegions()
      //if (!wdensities) {
        //get prior regions from chromosomes
        var regions_outfh = new File( outputstem + ".randomregions" )
        var regions_outfile  = new FilePrinter(regions_outfh)
        regions.map(r => regions_outfile.println( r._1 + "\t" + r._2.toString + "\t" + r._3.toString  )  )
        regions_outfile.closeBufferedWriter()
      //}
      //else {
        //print priorsample to file
        //var outfh = new File( outputstem + ".randomregiondensities" )
        //var outfile  = new FilePrinter(outfh)
        //val priorsample = regions.map(r => outfile.println(r._1 + "\t" + r._2.toString + "\t" + r._3.toString + "\t" + 
        //                                  priormfr.getRegion(r._1,r._2,r._3)._1.map(t => t.pileup).mkString("\t"))
        //                             )
        //outfile.closeBufferedWriter()
        //println("created prior file")
      //}
      System.exit(0)

    }

    if (mode == "density"){

      var regions:List[(String,Long,Long)] =  io.Source.fromFile(regionfn.get).getLines.toList.map(
                            a =>  (a.split("\\s+")(0), a.split("\\s+")(1).toLong, a.split("\\s+")(2).toLong)   )      
      //print priorsample to file
      var outfh = new File( outputstem + ".density" )
      var outfile  = new FilePrinter(outfh)
      var sampleids:Array[String] = Array()
      if (sharing_ind != -1 ){
        sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","") + "$" + a(sharing_ind + 1) ).toArray
      } else {
        sampleids = sampleinfo.map(a => a(0).split("/").last.replaceAll("\\.bam$","")).toArray
      }
      outfile.println( sampleids.mkString("\t")  )
      val priorsample = regions.map(r =>outfile.println(r._1 + "\t" + r._2.toString + "\t" + r._3.toString + "\t" + 
                                                        priormfr.getRegion(r._1,r._2,r._3)._1.map(t => t.pileup).mkString("\t")) )
      outfile.closeBufferedWriter()
      println("created density file")
      System.exit(0)

    }

    if (mode == "caller"){

      var sc_statsfh:Option[FilePrinter] = None
      var ilnc_statsfh:Option[FilePrinter] = None
      //var ilnsc_statsfh:Option[FilePrinter] = None
      var pu_statsfh:Option[FilePrinter] = None

      if (debug){
        var sc_statsjavafh = new File( outputstem + "/" + "simplecaller.statsout" )
        sc_statsfh  = Some( new FilePrinter(sc_statsjavafh)  )
        
        var ilnc_statsjavafh = new File( outputstem + "/" + "isinglncaller.statsout" )
        ilnc_statsfh  = Some( new FilePrinter(ilnc_statsjavafh)  )

        //var ilnsc_statsjavafh = new File( outputstem + "/" + ".isinglnscaller.statsout" )
        //ilnsc_statsfh  = Some( new FilePrinter(ilnsc_statsjavafh) )

        var pu_statsjavafh = new File( outputstem + "/" + "compdensities.statsout" )
        pu_statsfh  = Some( new FilePrinter(pu_statsjavafh) )
        //TODO: print header on this file
      }

      var pileups:List[SamplePileups] = List()

      var bedout = new BedOutputter(sampleinfo,sharing_ind, peakcallthreshold, outputstem, merged_outputfn)

      if (lnfn.isDefined) ics.fitLognormals(lnfn.get)  else  ics.fitLognormals()
      var sc = new SimpleCaller(ics,  sc_statsfh )
      var ilnc = new IsingLognormalCaller(ics,1000,100, 20, ilnc_statsfh )
      //var ilnsc = new IsingLognormalSharingCaller(ics,500, 50, 10, ilnsc_statsfh)

      var a = 0
      for( a <- 0 to chrs.length - 1){
        var chr = chrs(a)
        var mfr = new  MultiFileReader(Some(chr._1), Some(chr._2), Some(chr._3),sampleinfo,windowSize, bgSize, readmode, cuts )
        while (mfr.hasNextRegion()) {

          val pileupdets = mfr.getNextRegion() 
          var regionstr = chr._1 + ":" + pileupdets._2.toString + "-" +  pileupdets._3.toString
          pileups = pileupdets._1
          //if (pileups.map(a => a.pileup).sum > 50){
          if (debug){
            pu_statsfh.get.println( regionstr + "\t" +  pileups.map(a => a.pileup).mkString("\t")   )
          }
          var caller_output:Array[Double] = Array()
          var sharing_output:Option[(Double,Double)] = None

          var sc_output = sc.runcaller(pileups, regionstr)

          caller_output = sc_output.toArray

          //Only run sophisticated callers when we're in the corridor of uncertainty
          if (sc_output.toArray.filter(j => j >= isingcallerthresh && (1.0 - j) <= isingcallerthresh ).length > 0 || 
              sc_output.toArray.filter(j => j >= isingcallerthresh  ).length >= 2 ) {

            var ilnc_output = ilnc.runcaller(pileups, regionstr)

            //var ilnsc_output = ilnsc.runcaller(pileups, regionstr)


            //if (sharing_ind != -1 ){
            //  caller_output = ilnsc_output._1.toArray
            //  sharing_output = Some(ilnsc_output._2)
            //} else {
              caller_output = ilnc_output.toArray
            //}
          }
          //}
          bedout.addwindowresults(chr._1, pileupdets._2, pileupdets._3, caller_output, sharing_output)

        }
      }


      bedout.closeFiles()

      if (debug){
        ilnc_statsfh.get.closeBufferedWriter()
        //ilnsc_statsfh.get.closeBufferedWriter()
        sc_statsfh.get.closeBufferedWriter()
        pu_statsfh.get.closeBufferedWriter()
      }


      System.exit(0)


    }






  }



}


















