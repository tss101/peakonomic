lazy val root = (project in file(".")).
settings(
    name := "peakonomic",
    version := "0.1",
    scalaVersion := "2.11.6"
)


libraryDependencies  ++= Seq(
        // other dependencies here
        "org.scalanlp" %% "breeze" % "0.13.2",
        // native libraries are not included by default. add this if you want them (as of 0.7)
        // native libraries greatly improve performance, but increase jar sizes.
        "org.scalanlp" %% "breeze-natives" % "0.13.2",

        //breeze-viz for plotting
        "org.scalanlp" %% "breeze-viz" % "0.13.2",

        //htsjdk for reading bams
        "com.github.samtools" % "htsjdk" % "2.8.1"

        )

resolvers ++= Seq(
        // other resolvers here
        "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
        )



// [Required] Enable plugin and automatically find def main(args:Array[String]) methods from the classpath
enablePlugins(PackPlugin)

